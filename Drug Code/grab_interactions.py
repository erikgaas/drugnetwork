import re

f = open('sample.xml', encoding='utf-8', mode='r')
xml_file = f.read()
f.close()


drug_interactions = {}

def get_drug_indicies(xml_file):
	drug_indicies = []
	for i in range(len(xml_file) - len('<drug type') + 1):
		if xml_file[i:i+10] == '<drug type._\n':
			drug_indicies.append(i)
	return re.findall(r'<drug !?(\n)*', xml_file)



print(get_drug_indicies(xml_file))