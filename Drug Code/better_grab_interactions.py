# import xml.etree.ElementTree as ET

# parsed_drugs = ET.parse("sample.xml")

# print(ET)


import re
import pickle

# f = open('sample.xml', encoding='utf-8', mode='r')
# current_line = f.readline()
# current_line = f.readline()


value = ['drugbank', 'drug', 'name']
pair = ['drugbank', 'drug', 'drug-interactions', 'drug-interaction', 'name']

drug_interaction_pairs = {}


def extract_text_between_tags(line):
	line_summary = re.findall(r'<[^>]*>|<[^>]*/>|</[^>]*>', line)
	start = line.find(line_summary[0]) + len(line_summary[0])
	end = line.find(line_summary[-1])
	return line[start:end].strip()



def read_through_xml(xml_file):
	#g = open("output.txt", "w")
	current_line = "filler"
	current_drug = "nothing"
	f = open(xml_file, encoding='utf-8', mode='r')
	list_level = []
	while current_line != '':
		current_line = f.readline()
		if re.findall(r'<\? *xml(( +[a-z]* *= *"[^"]*")*| *)\?>', current_line) != []:
			pass
		else:
			line_summary = re.findall(r'<[^>]*>|<[^>]*/>|</[^>]*>', current_line)
			for i in line_summary:
				if re.findall(r'<[a-z\-]+ *(([a-zA-Z:-]* *= *"[^"]*")* *)*>', i) != []:
					
					word = re.findall(r'[a-z\-]+', i)[0]

					list_level.append(word)

					if list_level == value:
						current_drug = extract_text_between_tags(current_line)
						drug_interaction_pairs[current_drug] = []
					elif list_level == pair:
						drug_interaction_pairs[current_drug].append(extract_text_between_tags(current_line))



					#print("ADD", list_level)
					#g.write("ADD "+ str(list_level) + "\n")

				elif re.findall(r'</[^>]*>', i) != []:
					list_level = list_level[0:-1]
					#print("MINUS", list_level)
					#g.write("MINUS "+ str(list_level) + "\n")
	#g.close()
	f.close()





read_through_xml('drugbank.xml')
print(len(drug_interaction_pairs))
g = open("output.txt", encoding="utf-8", mode="w")
for i in drug_interaction_pairs:
	g.write(str(i) + " : " + str(drug_interaction_pairs[i]) + "\n")
g.close()



#pickle.dump( drug_interaction_pairs, open( "all_drug_int_graph.p", "wb" ) )


