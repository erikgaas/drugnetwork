/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package xmlparser;

import java.util.List;

/**
 *
 * @author jgiesler
 */
public class Drug{
    private String name;
    private List<Drug> interactions;
    //constructor
    public Drug(String inName){
        this.name = inName;
    }
    public void drugName(String assignedname){
        this.name = assignedname;
    }
    
    public String whoAmI(){
        return this.name;
    }
    
    public void addInteraction(Drug addDrug){
        this.interactions.add(addDrug);
    }
    public List<Drug> getInteractions(){
        return this.interactions;
    }

}
