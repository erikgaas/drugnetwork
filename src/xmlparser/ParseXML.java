/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package xmlparser;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author jgiesler
 */
public class ParseXML{
    private Document xmlDoc = null;
    private NodeList nList = null;
    private List<Drug> allDrugs = new ArrayList<>();
    public ParseXML(){ //constructor ..not normally good practice to have such a long task run in constructor
        try {
            File sourceDoc = new File ("/Users/jgiesler/Downloads/drugbank.xml");
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            this.xmlDoc = builder.parse(sourceDoc);
            this.xmlDoc.getDocumentElement().normalize();
            
            nList = this.xmlDoc.getElementsByTagName("drug");
            
            System.out.println("Parsed "+ nList.getLength() + "drugs");
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            System.exit(-1);
        }
    }
    public List<Drug> getDrugs(){ 
        if (!this.allDrugs.isEmpty()){
            return allDrugs;
        }else{
            for (int i = 0; i < nList.getLength(); i++){
                Node nNode = nList.item(i);
                
                if (nNode.getNodeType() == Node.ELEMENT_NODE){
                    Element el = (Element) nNode;
                    this.allDrugs.add(new Drug(el.getElementsByTagName("name").item(0).getTextContent()));
                    
                }
                
            }
        return this.allDrugs;
        }
    }
}

